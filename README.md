
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Package squirrels

<!-- badges: start -->
<!-- badges: end -->

Le but de squirrels est d’analyser les données sur les écureuils de New
York. Ce package a été créé dans le cadre d’une formation N2.

## Installation

You can install the development version of squirrels like so:

``` r
remotes::install_local(path = "~/squirrels_0.0.0.9000.tar.gz")
```

## Example

Avec ce package, on peut afficher la couleur choisie pour l’analyse :

``` r
library(squirrels)
get_message_fur_color(primary_fur_color="Black")
#> We will focus on Black squirrels
```

Ou encore vérifier que la couleur choisie existe bien :

``` r
check_primary_color_is_ok("Gray")
#> [1] TRUE
```

## Test pour Git
